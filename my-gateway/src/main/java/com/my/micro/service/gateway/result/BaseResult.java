package com.my.micro.service.gateway.result;

import lombok.Data;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Data
public class BaseResult<T> {

    private Integer code;

    private String message;

    public BaseResult(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    public static <T> BaseResult<T> failure(String  message){
        return new BaseResult<>(-1, message);
    }
}
