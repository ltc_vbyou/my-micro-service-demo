package com.my.micro.service.integral.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Slf4j
@RestController
@RequestMapping("/integral")
public class IntegralController {

    @Value("${server.port}")
    private Integer port;

    @GetMapping("/remain")
    public String remain(){
        log.info("积分服务被调用了");
        Random random = new Random();
        int i = random.nextInt(100);
        return "您当前的积分为：" + i + " 服务端口：" + port;
    }


    @GetMapping("/health")
    public Map<String,String> health(){
        Map<String, String> map = new HashMap<>(2);
        map.put("status", "UP");
        return map;
    }

}
