package com.my.micro.service.good.handler;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.my.micro.service.good.result.BaseResult;
import com.my.micro.service.good.result.ResponseUtil;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Component
public class MyBlockExceptionHandler implements BlockExceptionHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, BlockException e) throws Exception {
        final BaseResult<Void> result = BaseResult.fail("被限流了");
        ResponseUtil.sendMessage(response, result);
    }
}
