package com.my.micro.service.good.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Data
public class Order implements Serializable {

    private static final long serialVersionUID = 7216298966843238100L;

    private int id;

    private String name;
}
