package com.my.micro.service.feign;

import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.Retryer;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import feign.ribbon.RibbonClient;
import feign.slf4j.Slf4jLogger;
import feign.spring.SpringContract;

import java.util.concurrent.TimeUnit;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public class Spring4FeignDemo {

    public static void main(String[] args) {
        Spring4GoodsApi goodsApi = Feign.builder()
                // 使用spring4规范
                .contract(new SpringContract())
                // 使用jackson编解码
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                // okhttp客户端
                .client(new OkHttpClient())
                // 请求失败重试，默认最大5次
                .retryer(new Retryer.Default())
                // 请求超时配置
                .options(new Request.Options(10, TimeUnit.SECONDS, 60, TimeUnit.SECONDS, true))
                .requestInterceptor(new AuthFeignInterceptor())
                // 日志配置，将在请求前后打印日志
                .logger(new Slf4jLogger())
                // 日志等级配置，BASIC:只打印请求路径和响应状态码基本信息
                .logLevel(Logger.Level.BASIC)
                .target(Spring4GoodsApi.class, "http://localhost:8082");
        System.out.println(goodsApi.getGoods());
        System.out.println(goodsApi.list());
        goodsApi.save(new Goods().setName("banana"));
        goodsApi.delete("1");
    }
}
