package com.my.micro.service.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 统一拦截器
 *
 * @author Zijian Liao
 * @since 1.0.0
 */
public class AuthFeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        System.out.println("进入拦截器");
        template.header("token", "123456");
    }
}
