package com.my.micro.service.feign;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 使用spring4规范
 *
 * @author Zijian Liao
 * @since 1.0.0
 */
public interface Spring4GoodsApi {

    @GetMapping("/goods/get-goods")
    Goods getGoods();

    @GetMapping("/goods/list")
    List<Goods> list();

    @PostMapping(value = "/goods/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    void save(Goods goods);

    @DeleteMapping("/goods")
    void delete(@RequestParam(value = "id") String id);
}
