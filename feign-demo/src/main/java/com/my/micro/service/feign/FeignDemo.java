package com.my.micro.service.feign;

import feign.Feign;
import feign.codec.StringDecoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
public class FeignDemo {

    public static void main(String[] args) {
        GoodsApi goodsApi = Feign.builder()
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(GoodsApi.class, "http://localhost:8082");
        System.out.println(goodsApi.getGoods());
        System.out.println(goodsApi.list());
        goodsApi.save(new Goods().setName("banana"));
        goodsApi.delete("1");
    }
}
