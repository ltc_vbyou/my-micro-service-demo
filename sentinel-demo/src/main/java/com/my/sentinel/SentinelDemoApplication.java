package com.my.sentinel;

import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@SpringBootApplication
public class SentinelDemoApplication {

    public static void main(String[] args) {
//        initFlowRules();
        SpringApplication.run(SentinelDemoApplication.class, args);
    }

    private static void initFlowRules(){
        // 定义流控规则
        FlowRule rule = new FlowRule();
        // 资源名与需要限流的资源名相同
        rule.setResource("hello");
        // 设置限流方式为QPS
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        // 设置QPS为20
        rule.setCount(2);
        // 加载规则
        FlowRuleManager.loadRules(Collections.singletonList(rule));
    }
}
