package com.my.sentinel.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Service
public class FooServiceImpl implements FooService {

    @SentinelResource(value = "fooService-hello", blockHandler = "exceptionHandler", fallback = "helloFallback")
    @Override
    public String hello(String name){
        return "hello " + name;
    }

    public String helloFallback(String name) {
        return "被降级了";
    }

    public String exceptionHandler(String name, BlockException ex) {
        // Do some log here.
        ex.printStackTrace();
        return "被限流了";
    }

}
