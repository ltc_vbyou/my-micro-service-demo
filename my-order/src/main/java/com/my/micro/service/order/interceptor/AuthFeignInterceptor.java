package com.my.micro.service.order.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;

/**
 * 统一拦截器
 *
 * @author Zijian Liao
 * @since 1.0.0
 */
public class AuthFeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        System.out.println("进入拦截器");
        template.header("token", "123456");
    }
}
