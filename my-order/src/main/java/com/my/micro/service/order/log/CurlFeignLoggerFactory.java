package com.my.micro.service.order.log;

import feign.Logger;
import feign.slf4j.Slf4jLogger;
import org.springframework.cloud.openfeign.DefaultFeignLoggerFactory;

/**
 * curl feignlogger
 *
 * @author Zijian Liao
 * @since 1.0.0
 */
public class CurlFeignLoggerFactory extends DefaultFeignLoggerFactory {

    public CurlFeignLoggerFactory(Logger logger) {
        super(logger);
    }

    @Override
    public Logger create(Class<?> type) {
        return new CurlLogger(type);
    }
}
