package com.my.micro.service.order.result;

import lombok.Data;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Data
public class BaseResult<T> {

    private Integer code;

    private String message;

    private T data;

    public BaseResult(){

    }

    public BaseResult(Integer code, String message, T data){
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> BaseResult<T> success(T data){
        return new BaseResult<>(0, "成功", data);
    }
}
