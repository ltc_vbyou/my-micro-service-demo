package com.my.micro.service.order.config;

import com.my.micro.service.order.decode.BaseResultDecode;
import com.my.micro.service.order.log.CurlFeignLoggerFactory;
import feign.codec.Decoder;
import feign.optionals.OptionalDecoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignLoggerFactory;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Zijian Liao
 * @since
 */
@Configuration
public class DecodeConfiguration {

    @Bean
    public Decoder feignDecoder(ObjectFactory<HttpMessageConverters> messageConverters) {
        return new OptionalDecoder(
                new BaseResultDecode(new SpringDecoder(messageConverters)));
    }

    @Bean
    public FeignLoggerFactory curlFeignLoggerFactory(){
        return new CurlFeignLoggerFactory(null);
    }
}
