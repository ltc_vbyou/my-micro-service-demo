package com.my.micro.service.order.feign;

import com.my.micro.service.order.domain.Goods;
import com.my.micro.service.order.result.BaseResult;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Zijian Liao
 * @since 1.0.0
 */
@Slf4j
@Component
public class GoodsApiFallback implements FallbackFactory<GoodsApi> {
    @Override
    public GoodsApi create(Throwable throwable) {
        log.error(throwable.getMessage(), throwable);
        return new GoodsApi() {

            @Override
            public Goods getGoods() {
                throw new RuntimeException("调用商品服务发生异常");
            }

            @Override
            public BaseResult<Goods> getGoods(Goods goods) {
                return null;
            }

            @Override
            public List<Goods> list() {
                return null;
            }

            @Override
            public void save(Goods goods) {

            }

            @Override
            public void delete(String id) {

            }
        };
    }
}
